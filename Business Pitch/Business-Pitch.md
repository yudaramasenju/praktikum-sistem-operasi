# Web Page Sederhana Menggunakan Docker

Docker adalah platform open-source yang memungkinkan kita untuk mengemas, mengirim, dan menjalankan aplikasi dalam lingkungan yang terisolasi yang disebut "container". Container memungkinkan aplikasi dan dependensinya untuk diisolasi dari lingkungan host, sehingga menjadikannya lebih portabel, skalabel, dan mudah dikelola.

Pembuatan web service atau web page menggunakan Docker memungkinkan kita untuk mengisolasi aplikasi web Anda, bersama dengan semua dependensinya, dalam container Docker. Dengan demikian, dapat menghindari masalah dependensi, meningkatkan portabilitas, mempercepat proses pengiriman aplikasi, dan memastikan konsistensi antara lingkungan pengembangan dan produksi.

# Nomor 1 Pembuatan Docker Image
---

Langkah pertama membuat file ``Dockerfile`` dengan menggunakan perintah ``touch``. Didalam file ini berisi :
 
```dockerfile

# Base image
FROM debian:bullseye-slim

# Install dependencies
RUN apt-get update && apt-get install -y git neovim netcat python3-pip

# Copy web service files
COPY alquran.py home/alquran/alquran-viewer.py
COPY requirements.txt home/alquran/requirements.txt

# Install Python dependencies
RUN pip3 install --no-cache-dir -r /home/alquran/requirements.txt

# Set working directory
WORKDIR /home/alquran

# Expose port
EXPOSE 25146

# Run the application
CMD ["streamlit", "run", "--server.port=25146", "alquran.py" ]
```

-  instruksi ``FROM`` digunakan untuk menentukan base image yang akan digunakan dalam pembuatan image Docker. Base image adalah image dasar yang akan digunakan sebagai titik awal dalam membangun image Docker

- instruksi ``RUN`` digunakan untuk mengeksekusi perintah-perintah di dalam konteks kontainer saat proses pembuatan image Docker. 
``apt-get update`` digunakan untuk mengupdate daftar paket yang tersedia di dalam sistem operasi base image. dan ``apt-get install`` digunakan untuk menginstall beberapa paket yang diperlukan.

- instruksi ``COPY`` digunakan untuk menyalin file atau direktori dari host yang sedang menjalankan perintah docker build ke dalam image Docker yang sedang dibangun.

- instruksi ``WORKDIR`` digunakan untuk mengatur direktori kerja (working directory) di dalam kontainer Docker yang akan dibangun.

- instruksi ``EXPOSE`` digunakan untuk menentukan port yang akan di-"expose" (tertutup) saat menjalankan kontainer berdasarkan image Docker yang sedang dibangun.

-  instruksi ``CMD`` digunakan untuk menentukan perintah default yang akan dijalankan saat kontainer Docker berjalan dari image yang dibangun.


# Nomor 2 Pembuatan Web Page/Web Service
---

Web page yang saya buat menggunakan bahasa pemgrograman Python dan framework streamlit dengan dependencies yang tertera di dockerfile tadi.

![WEBPAGE](Assets/WEBPAGE.png)

Untuk source web page nya [disini](src/alquran.py)

Web page bisa di akses di ``http://135.181.26.148:25146/`` atau [click here](http://135.181.26.148:25146/)

# Nomor 3 Docker Compose
---

Setelah membangun ``Docker Image`` dengan [Dockerfile](src/Dockerfile), Selanjutnya adalah membuat file ``docker-compose.yml`` untuk kontainerisasi program. Konfigurasi contoh sebagai berikut:

```dockerfile
version: "3"
services:
  alquran-viewer:
    image: yudaristian/alquran:1.0
    ports:
    - "25146:25146"
    networks:
    - niat-yang-suci
    volumes:
    - /home/praktikumc/1217050146/alquran:/home/alquran

networks:
  niat-yang-suci:
    external: true
```

Setelah ``docker-compose.yml`` berhasil dibuat, langkah Selanjutnya adalah menggunakan perintah dibawah ini untuk membangun Docker Image :

```dockerfile
docker build -t alquran:1.0 .
```

**- Mengganti tag Docker Image**

Dalam Docker, "tag" digunakan untuk memberikan label atau versi pada sebuah image. Tag ini membantu dalam mengidentifikasi dan mengelola versi dari image yang dibangun. 

```dockerfile
docker tag alquran:1.0 yudaristian/alquran:1.0
```

**- Buat akun dockerhub**

Atau [klik di sini](https://hub.docker.com/)

![DOCKERHUB](Assets/dockerhub.png)

**- Login**
Lakukan dengan ``docker login`` dan kita akan diminta memasukkan username dan password, isi dengan akun yang kita buat sebelumnya.

**- Docker Push**

Perintah docker push digunakan untuk mengunggah (mengirim) image Docker yang telah Anda bangun ke Docker Registry, seperti Docker Hub atau registry privat.

```docker file
docker push yudaristian/alquran:1.0
```

# Nomor 4 Deskripsi Project
---

**- Deskripsi Project**

Aplikasi ini dibangun menggunakan bahasa pemrograman python dan framework streamlit. Aplikasi Web ini berjudul ```Alquran Surah Viewer``` berfungsi untuk melihat dan menampilkan informasi mengenai surat surat dalam Al-quran. Terdapat fitur pencarian surat, ini memungkinan user untuk mencari sebagian nama surat atau nama lengkap surat tersebut. Kemudian akan ditampilkan informasi mengenai surat tersebut yang memuat nama surat,arti surat,jumlah ayat,keterangan dan pemutar audio dari surat tersebut.

Projek Al-Quran Surah Viewer yang  dibuat dapat ini untuk membantu mengatasi masalah yang dapat memengaruhi keimanan dan ketaqwaan. Diantaranya adalah Akses yang Mudah dan Cepat, dengan adanya fitur pencarian surat  pengguna dapat dengan mudah menemukan surat Al-Quran yang ingin mereka baca atau pelajari. Aplikasi web ini juga berfungsi sebagai sarana pendidikan dan pengajaran.

API yang digunakan dari ``"https://al-quran-8d642.firebaseio.com/data.json?print=pretty"`` untuk mengambil data JSON yang berisi informasi tentang surat-surat Al-Quran.

**- Peran Sistem Operasi pada Containerization**

Dalam proses containerization, sistem operasi digunakan sebagai dasar untuk menjalankan dan mengisolasi container. Sistem operasi host menyediakan lingkungan yang diperlukan untuk menjalankan container dan harus mendukung teknologi kontainerisasi seperti Docker atau Kubernetes. Sistem operasi host harus memiliki kernel yang mendukung fitur-fitur seperti cgroups dan namespaces yang memungkinkan manajemen sumber daya dan isolasi yang kuat antara container yang berjalan. Selain itu, images container yang digunakan dalam containerization berisi sistem operasi yang diperlukan untuk menjalankan aplikasi di dalam container, termasuk dependensi dan file sistem yang diperlukan. 

**- Containerization membantu mempermudah pengembangan aplikasi**

Containerization merupakan pendekatan dalam pengembangan aplikasi yang dapat membantu mempermudah proses pengembangan.

1. Lingkungan yang Konsisten: Dengan containerization, pengembang dapat menciptakan lingkungan pengembangan yang konsisten di seluruh siklus hidup aplikasi. Container berisi semua dependensi dan konfigurasi yang diperlukan untuk menjalankan aplikasi, termasuk sistem operasi, runtime, perpustakaan, dan variabel lingkungan. 

2. Skalabilitas dan Isolasi: Containerization memungkinkan pengembang untuk dengan mudah memperluas atau mengurangi kapasitas aplikasi dengan cepat.

**- Apa itu DevOps, Bagaimana DevOps membantu pengembangan aplikasi**

DevOps adalah pendekatan kolaboratif yang menggabungkan praktik pengembangan perangkat lunak (Development) dan operasi TI (Operations) untuk mempercepat pengembangan dan penyebaran aplikasi dengan kualitas yang lebih baik. DevOps bertujuan untuk memperkuat komunikasi, kerjasama, dan integrasi antara tim pengembangan dan tim operasi dalam siklus pengembangan aplikasi.

DevOps membantu pengembangan aplikasi dalam beberapa cara:

1. Percepatan Time-to-Market: Dengan menerapkan praktik DevOps, pengembang dapat melakukan pengiriman perangkat lunak lebih cepat ke pasar. 

2. Meningkatkan Kualitas Aplikasi: Tim pengembangan dan operasi bekerja bersama untuk membangun pipa garis (pipeline) pengujian yang kuat dan melakukan pemantauan aplikasi secara kontinu. Dengan demikian, masalah dapat dideteksi lebih awal, dan tindakan perbaikan dapat diambil dengan cepat. 

3. Kolaborasi dan Komunikasi yang Meningkat: DevOps mendorong kolaborasi dan komunikasi yang erat antara tim pengembangan dan tim operasi. Mereka bekerja bersama-sama dalam proses pengembangan, pengujian, dan penyebaran aplikasi. 

4. Skalabilitas dan Reliabilitas: DevOps memungkinkan pengembang dan operator untuk mengelola dan mengotomatiskan infrastruktur dan pengaturan aplikasi dengan baik. Penggunaan manajemen konfigurasi dan alat otomatisasi membantu meningkatkan skalabilitas dan reliabilitas aplikasi.

**- Penerapan DevOps** 

**eBay** adalah platform perdagangan elektronik global yang menghubungkan jutaan penjual dan pembeli. Mereka telah mengadopsi Docker sebagai bagian dari pendekatan DevOps mereka. Berikut ini adalah beberapa penerapan DevOps dengan Docker yang dilakukan oleh eBay:

1. Pengembangan Lokal yang Seragam: eBay menggunakan Docker untuk menyediakan lingkungan pengembangan yang seragam bagi seluruh tim pengembang. Setiap pengembang dapat menjalankan aplikasi di lingkungan Docker yang sama, dengan semua dependensi dan konfigurasi yang telah ditentukan sebelumnya.

1. Continuous Integration dan Deployment:  Setiap kali ada perubahan kode, aplikasi diuji dalam lingkungan Docker menggunakan alat otomatisasi dan unit tes. Jika pengujian berhasil, Docker image baru dibangun dan diterapkan ke produksi dengan menggunakan alat otomatisasi dan orkestrasi seperti Kubernetes.

3. Skalabilitas dan Pemulihan Bencana: eBay menggunakan Docker dan orkestrasi kontainer seperti Kubernetes. Mereka dapat dengan mudah mengelola dan menyebarluaskan aplikasi di lingkungan yang dapat diandalkan dan dapat diperluas. Jika ada kegagalan pada salah satu kontainer atau node, sistem orkestrasi akan secara otomatis mendeteksi dan menggantikan kontainer yang rusak. Ini memastikan ketersediaan dan keandalan tinggi bagi pengguna eBay.

4. Pengujian dan Reproduksi Masalah: eBay menggunakan Docker untuk mengisolasi dan mereproduksi masalah yang terjadi dalam lingkungan pengembangan atau produksi. Mereka dapat dengan mudah membuat lingkungan Docker yang mirip dengan produksi dan mengisolasi masalah yang muncul. Hal ini memungkinkan tim untuk menganalisis masalah dengan cepat dan mencari solusi tanpa mengganggu lingkungan produksi

# Nomor 5 Demonstrasi Web Page/ Web Service
---

[Youtube](https://youtu.be/0j7IPF8UuOk)

# Nomor 6 Publikasi Docker Image
---

[Docker Hub](https://hub.docker.com/u/yudaristian)
