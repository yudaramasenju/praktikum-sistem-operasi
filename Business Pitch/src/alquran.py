import streamlit as st
import requests

# Mendapatkan data JSON dari URL
response = requests.get("https://al-quran-8d642.firebaseio.com/data.json?print=pretty")
data = response.json()

# Mengatur judul halaman web
st.title("Al-Quran Surah Viewer")

# Membuat input text untuk mencari surat
search_query = st.text_input("Cari Surat")

# Memfilter data berdasarkan input pencarian
filtered_data = []
for surah in data:
    if search_query.lower() in surah["nama"].lower():
        filtered_data.append(surah)

# Menampilkan hasil pencarian
st.write(f"Menampilkan {len(filtered_data)} surat yang cocok dengan pencarian:")
for surah in filtered_data:
    st.markdown(f"### {surah['nama']}")
    st.markdown(f"**Arti**: {surah['arti']}")
    st.markdown(f"**Ayat**: {surah['ayat']}")
    st.markdown("**Keterangan**: ")
    st.markdown(surah['keterangan'], unsafe_allow_html=True)
    st.audio(surah['audio'], format='audio/mp3')
    st.write("---")
