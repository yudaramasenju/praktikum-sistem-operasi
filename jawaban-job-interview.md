# Praktikum Sistem Operasi

# Nomor 1 Resource Monitoring
RAM
---
Menggunakan Perintah `free` untuk mengecek kondisi RAM total, yang sedang digunakan maupun yang tidak digunakan dan yang digunakan sebagai swap.

<pre><code>praktikumc@praktikumsistemoperasi ~
 % free
               total        used        free      shared  buff/cache   available
Mem:         2030420      279572      233916         660     1516932     1574244
Swap:              0           0           0
</pre></code>

Kemudian untuk melihat perintah `free` bisa melakukan apa saja atau melihat deskripsi dari argumen tersebut. Bisa menggunakan argumen `free --help`
<pre><code>praktikumc@praktikumsistemoperasi ~
 % free --help

Usage:
 free [options]

Options:
 -b, --bytes         show output in bytes
     --kilo          show output in kilobytes
     --mega          show output in megabytes
     --giga          show output in gigabytes
     --tera          show output in terabytes
     --peta          show output in petabytes
 -k, --kibi          show output in kibibytes
 -m, --mebi          show output in mebibytes
 -g, --gibi          show output in gibibytes
     --tebi          show output in tebibytes
     --pebi          show output in pebibytes
 -h, --human         show human-readable output
     --si            use powers of 1000 not 1024
 -l, --lohi          show detailed low and high memory statistics
 -t, --total         show total for RAM + swap
 -s N, --seconds N   repeat printing every N seconds
 -c N, --count N     repeat printing N times, then exit
 -w, --wide          wide output

     --help     display this help and exit
 -V, --version  output version information and exit

For more details see free(1).</pre></code>

CPU
---
Kita bisa melihat proses yang sedang berjalan di CPU dengan menggunakan perintah `top` atau `ps`
![top](https://gitlab.com/yudaramasenju/praktikum-sistem-operasi/-/raw/main/Screenshot/CPU.png)

atau `ps -aux`
<pre><code>praktikumc@praktikumsistemoperasi ~
 % ps -aux
USER         PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
root           1  0.0  0.5 163920 10336 ?        Ss   Apr28   0:38 /lib/systemd/systemd --system --deserialize 36
root           2  0.0  0.0      0     0 ?        S    Apr28   0:00 [kthreadd]
root           3  0.0  0.0      0     0 ?        I<   Apr28   0:00 [rcu_gp]
root           4  0.0  0.0      0     0 ?        I<   Apr28   0:00 [rcu_par_gp]
root           6  0.0  0.0      0     0 ?        I<   Apr28   0:00 [kworker/0:0H-events_highpri]
root           8  0.0  0.0      0     0 ?        I<   Apr28   0:00 [mm_percpu_wq]
root           9  0.0  0.0      0     0 ?        S    Apr28   0:00 [rcu_tasks_rude_]
root          10  0.0  0.0      0     0 ?        S    Apr28   0:00 [rcu_tasks_trace]
root          11  0.0  0.0      0     0 ?        S    Apr28   0:17 [ksoftirqd/0]
root          12  0.0  0.0      0     0 ?        I    Apr28   1:27 [rcu_sched]
root          13  0.0  0.0      0     0 ?        S    Apr28   0:07 [migration/0]
root          15  0.0  0.0      0     0 ?        S    Apr28   0:00 [cpuhp/0]
root          16  0.0  0.0      0     0 ?        S    Apr28   0:00 [cpuhp/1]
root          17  0.0  0.0      0     0 ?        S    Apr28   0:07 [migration/1]
root          18  0.0  0.0      0     0 ?        S    Apr28   0:02 [ksoftirqd/1]
root          20  0.0  0.0      0     0 ?        I<   Apr28   0:00 [kworker/1:0H-events_highpri]
root          23  0.0  0.0      0     0 ?        S    Apr28   0:00 [kdevtmpfs]
root          24  0.0  0.0      0     0 ?        I<   Apr28   0:00 [netns]
root          25  0.0  0.0      0     0 ?        S    Apr28   0:00 [kauditd]
root          26  0.0  0.0      0     0 ?        S    Apr28   0:00 [khungtaskd]
root          27  0.0  0.0      0     0 ?        S    Apr28   0:00 [oom_reaper]
root          28  0.0  0.0      0     0 ?        I<   Apr28   0:00 [writeback]</pre></code>

Disk
---
Gunakan perintah `df` untuk melihat statistik dari disk.
<pre><code>praktikumc@praktikumsistemoperasi ~
 % df
Filesystem     1K-blocks    Used Available Use% Mounted on
udev              997040       0    997040   0% /dev
tmpfs             203044     680    202364   1% /run
/dev/vda1       41111748 3958156  35445116  11% /
tmpfs            1015208       0   1015208   0% /dev/shm
tmpfs               5120       0      5120   0% /run/lock
/dev/vda15        126678   10900    115778   9% /boot/efi
tmpfs             203040       0    203040   0% /run/user/1001
praktikumc@praktikumsistemoperasi ~</pre></code>

atau `df -h` untuk dibaca lebih mudah informasi dari statistik yang ditampilkan.
<pre><code>praktikumc@praktikumsistemoperasi ~
 % df  -h
Filesystem      Size  Used Avail Use% Mounted on
udev            974M     0  974M   0% /dev
tmpfs           199M  680K  198M   1% /run
/dev/vda1        40G  3.8G   34G  11% /
tmpfs           992M     0  992M   0% /dev/shm
tmpfs           5.0M     0  5.0M   0% /run/lock
/dev/vda15      124M   11M  114M   9% /boot/efi
tmpfs           199M     0  199M   0% /run/user/1001</pre></code>

# Nomor 2 Manajemen Program
Memonitor Program Yang Sedang berjalan
---
Seperti perintah yang sebelumnya untuk memonitor atau mengecek program yang sedang berjalan kita bisa menggunakan perintah atau argumen `top` atau `ps -aux`

Menghentikan program yang sedang berjalan
---
Perintah `kill` digunakan untuk menghentikan program yang sedang berjalan berdasarkan PID nya.
```
kill <PID> 
```

Otomasi Perintah Dengan Shell Script
---
Pertama cek bash yang digunakan dengan menggunakan perintah `which bash`, lalu buat file `.sh`

```
praktikumc@praktikumsistemoperasi ~/1217050146-blackjack
 % which bash
/usr/bin/bash
```

kemudian buat file `.sh`

```
praktikumc@praktikumsistemoperasi ~/1217050146-blackjack
 % touch contoh-project.sh
 ```
Buka file tersebut dengan menggunakan perintah `vim`,pada baris pertama gunakan `#! /bin/bash`,untuk menentukan interpreter untuk mengeksekusi skrip, dalam hal ini Bash shell.

Masuk mode insert dengan tombol i untuk melakukan peng-kodean, untuk kembali ke mode visual tekan tombol esc dan ketik :wq untuk keluar dan menyimpan perubahan.

```
praktikumc@praktikumsistemoperasi ~/1217050146-blackjack
 % vim contoh-project.sh

 #! /bin/bash
 ```


kita gunakan Perintah `chmod` untuk mengubah permission dari file contoh-project.sh. Dengan menambahkan `+x` maka kita memberikan perintah untuk memberikan hak eksekusi kepada pengguna.
```
praktikumc@praktikumsistemoperasi ~/1217050146-blackjack
 % chmod +x contoh-project.sh
```
Kemudian untuk menjalankan atau mengeksekusi file `.sh` tadi kita gunakan perintah.
```
bash contoh-project.sh
```

Penjadwalan eksekusi program dengan CRON
---
Gunakan perintah `crontab -e`untuk edit crontab
```
crontab -e
```

Tambahkan perintah cron job via crontab
```
* * * * * /home/praktikumc/1217050146-blackjack/contoh-project.sh
```
program shell script diatas akan dijalankan setiap menit.
# Nomor 3 Manajemen Network
Mengakses sistem operasi pada jaringan menggunakan SSH
---
Kita bisa mengakses  sistem operasi pada jaringan menggunakan SSH dengan cara memasukkan perintah di CLI.

1. Akses dengan ip(only)
```
ssh<username>@<public-ip>
```
Hasil login via CLI terlihat seperti dibawah ini
```
C:\Users\LENOVO>ssh praktikumc@103.82.93.37
praktikumc@103.82.93.37's password:
 --------------------------------------------
 ----- IDCloudHost Debian 11 (bullseye) -----
 --------------------------------------------
 * Panduan:  https://idcloudhost.com/panduan
 --------------------------------------------
Linux praktikumsistemoperasi 5.10.0-21-amd64 #1 SMP Debian 5.10.162-1 (2023-01-21) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Fri May 12 03:02:32 2023 from 103.55.33.184
praktikumc@praktikumsistemoperasi ~
 %
 ```
Monitor program yang menggunakan network
---
kita bisa menggunakan perintah `netstat` adalah perintah pada sistem operasi yang digunakan untuk menampilkan berbagai informasi terkait koneksi jaringan pada komputer. 
```
praktikumc@praktikumsistemoperasi ~/1217050146-blackjack
 % netstat
Active Internet connections (w/o servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State      
tcp        0      0 10.29.106.129:ssh       S010644a56e7cbc36:57796 FIN_WAIT2  
tcp        0      0 10.29.106.129:ssh       103.55.33.178:59151     ESTABLISHED
tcp        0      0 localhost:33576         localhost:43751         ESTABLISHED
tcp       28      0 localhost:33566         localhost:43751         ESTABLISHED
tcp        0      0 localhost:43751         localhost:33566         ESTABLISHED
tcp        0      0 localhost:41825         localhost:35600         CLOSE_WAIT 
tcp        0      0 localhost:43751         localhost:33576         ESTABLISHED
tcp        0      0 10.29.106.129:ssh       159.223.187.62:56730    TIME_WAIT  
tcp        0    109 10.29.106.129:ssh       103.55.33.184:50813     FIN_WAIT1  
tcp        0    517 10.29.106.129:50164     104.21.17.252:https     ESTABLISHED
tcp        0    160 10.29.106.129:ssh       103.55.33.178:59173     ESTABLISHED
Active UNIX domain sockets (w/o servers)
Proto RefCnt Flags       Type       State         I-Node   Path
unix  2      [ ]         DGRAM                    9511     /run/systemd/journal/syslog
unix  10     [ ]         DGRAM                    9517     /run/systemd/journal/dev-log
unix  6      [ ]         DGRAM                    9519     /run/systemd/journal/socket
```
Operasi HTTP Client
---
Kita bisa gunakan perintah `curl` untuk mendownload dengan memasukkan url terkait.
``` 
curl <spesipic url>
``` 
atau bisa juga menggunakan perintah `wget`
```
wget https://archives.bulbagarden.net/media/upload/thumb/f/fb/0001Bulbasaur.png/120px-0001Bulbasaur.png
```
# Nomor 4 Manajemen File dan Folder
Navigasi
---
Berikut ini beberapa perintah yang dapat digunakan untuk membantu proses navigasi di directory.
1. cd ; (Change Directory) // Perintah ini digunakan untuk berpindah ke direktori lain. contoh :
```
cd ./1217050146-blackjack
```
2. ls; (list) // Perintah ini digunakan untuk menampilkan daftar dan folder pada directory saat ini.
```
ls
```
3. pwd; (Print Working Directory) // Perintah ini digunakan untuk menampilkan direktori kerja saat ini di dalam sistem operasi.
```
pwd
```
CRUD File And Folder
---
- Penggunaan Editor

Gunakan perintah`vim`untuk membuka salah satu editor teks yang paling populer di lingkungan linux dan unix.
```
vim nama-file.format-file
```

- CRUD

Beberapa perintah untuk CRUD diantaranya :
```
mkdir   // Digunakan untuk membuat folder baru
touch   // Digunakan untuk membuat file baru
rm      // Digunakan untuk menghapus file
rm -f   // Digunakan untuk menghapus folder dan seluruh isinya
mv      // Digunakan untuk memindahkan file atau direktori
cp      // Digunakan untuk menyalin file atau folder
echo    // Digunakan untuk menulis ke file 
cat     // Digunakan untuk melihat isi file
```
Manajemen Ownership dan Akses file dan folder
---
Terdapat dua perintah dasar dalam Manajemen Ownership yakni `chown` dan `chmod`
- **chown**

Perintah ini digunakan untuk mengubah kepemilikan pengguna dan grup
```
chown user:group nama-file
```
- **chmod**

Perintah ini digunakan untuk memodifikasi permission dari suatu file.
```
chmod [option] <file>
```
untuk melihat ketentuan `option` bisa dilihat [disini](https://gitlab.com/marchgis/march-ed/2023/courses/if214009-praktikum-sistem-operasi/-/tree/main/linux#file-folder-mode-and-ownership)

Pencarian File Dan Folder
---
Terdapat beberapa perintah untuk mencari file dan folder seperti `find` `locate` dan `grep`
- Pencarian dengan find

```
find <directory> <conditions> <actions>
```

untuk melihat kondisi yang akan dipilih bisa dilihat [disini](https://devhints.io/find) untuk lebih lengkapnya.

Kompresi Data
---
Perintah yang digunakan yaitu `tar`

```
tar [options] [archives-file] [file or directory to be archives]
```

untuk melihat `options` lebih lengkap bisa dilihat [disini](https://www.geeksforgeeks.org/tar-command-linux-examples/)

# Nomor 5 Shell Script
Source Code [Shell Script](https://gitlab.com/yudaramasenju/praktikum-sistem-operasi/-/blob/main/Shell%20Script/monitoring-linux.sh)
# Nomor 6 Youtube
Link Youtube [Link]()
# Nomor 7 Maze Game
Maze Game Yang saya buat bisa dibuka [disini](https://maze.informatika.digital/maze/blackjack/)
# Nomor 8 Initiative in Project
Penggunaan Tools
---
Tools yang digunakan adalah ****Visual Studio**** Code dengan menggunakan extension **Remote SSH**

**extension**
![SSH](Screenshot/Screenshot_2023-05-13_085745.png)

**Tampilan VSCode**
![VSCODE](Screenshot/Screenshot_2023-05-13_085503.png)
