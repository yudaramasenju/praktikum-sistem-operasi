#!/bin/bash

# Skrip ini akan memantau penggunaan sumber daya pada sistem Linux menggunakan perintah free, top, dan df

# Fungsi untuk memeriksa penggunaan CPU
check_cpu() {
    echo "$(date): CPU usage:"
    top -b -n 1 | grep '%Cpu'
    echo
}

# Fungsi untuk memeriksa penggunaan memori
check_memory() {
    echo "$(date): Memory usage:"
    free -m
    echo
}

# Fungsi untuk memeriksa penggunaan ruang disk pada partisi /dev/sda1
check_disk() {
    echo "$(date): Disk usage:"
    df -h /dev/sda1
    echo
}

# Panggil fungsi untuk memeriksa penggunaan sumber daya
check_cpu
check_memory
check_disk

